package io.gitlab.donespeak.spring.boot.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 19:45
 */
@Data
@ConfigurationProperties(prefix = LoginProperties.LOGIN_PREFIX)
public class LoginProperties {
    public static final String LOGIN_PREFIX = "donespeak.spring.login";

    private String type;
}
