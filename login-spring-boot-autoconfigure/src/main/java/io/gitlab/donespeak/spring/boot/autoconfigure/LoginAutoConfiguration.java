package io.gitlab.donespeak.spring.boot.autoconfigure;

import io.gitlab.donespeak.spring.boot.login.service.config.LoginServiceConfig;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 19:41
 */
@Configuration
@EnableConfigurationProperties(LoginProperties.class)
@Import(LoginServiceConfig.class)
public class LoginAutoConfiguration {
}
