package login.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginSpringBootSampleWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoginSpringBootSampleWebApplication.class, args);
	}

}
