package login.web.controller;

import login.web.domain.City;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 21:18
 */
@RestController
@RequestMapping("citys")
public class CityController {

    @GetMapping("")
    public List<City> listCity() {
        return Arrays.asList(new City());
    }
}
