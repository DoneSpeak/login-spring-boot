package login.web.controller;

import io.gitlab.donespeak.spring.boot.login.service.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.donespeak.spring.boot.login.service.service.UserSerivce;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 21:26
 */
@RestController
@RequestMapping("/users")
public class UserController {
    //
    // @Autowired
    private UserSerivce userSerivce;

    @GetMapping("/{userId:\\d+}")
    public User getUserById(@PathVariable long userId) {
        return userSerivce.getUserById(userId);
    }

}
