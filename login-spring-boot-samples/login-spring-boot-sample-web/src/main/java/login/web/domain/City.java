package login.web.domain;

import lombok.Data;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 21:19
 */
@Data
public class City {
    private String name;
}
