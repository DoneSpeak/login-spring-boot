package login.web.domain;

import lombok.Data;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 21:17
 */
@Data
public class User {
    private String username;
}
