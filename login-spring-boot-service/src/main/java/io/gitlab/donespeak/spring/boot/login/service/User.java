package io.gitlab.donespeak.spring.boot.login.service;

import lombok.Data;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 18:33
 */
@Data
public class User {
    private long userId;
    private String username;
}
