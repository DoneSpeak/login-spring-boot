package io.gitlab.donespeak.spring.boot.login.service.service;

import io.gitlab.donespeak.spring.boot.login.service.User;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 21:24
 */
public interface UserSerivce {
    User getUserById(long userId);
}
