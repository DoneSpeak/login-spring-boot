package io.gitlab.donespeak.spring.boot.login.service.service.impl;

import io.gitlab.donespeak.spring.boot.login.service.User;
import io.gitlab.donespeak.spring.boot.login.service.service.UserSerivce;
import org.springframework.stereotype.Service;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 21:24
 */
@Service
public class UserServiceImpl implements UserSerivce {

    @Override
    public User getUserById(long userId) {
        User user = new User();
        user.setUserId(userId);

        return user;
    }
}
