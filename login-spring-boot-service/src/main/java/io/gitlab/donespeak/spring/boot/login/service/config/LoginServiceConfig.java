package io.gitlab.donespeak.spring.boot.login.service.config;

import io.gitlab.donespeak.spring.boot.login.service.PackageMarker;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 19:55
 */
@Configuration
@ComponentScan(basePackageClasses = {PackageMarker.class})
public class LoginServiceConfig {
}
