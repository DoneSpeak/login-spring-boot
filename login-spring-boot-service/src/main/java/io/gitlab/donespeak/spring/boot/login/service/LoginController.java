package io.gitlab.donespeak.spring.boot.login.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Yang Guanrong
 * @date 2019/11/22 18:29
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @GetMapping
    public User login(User user) {
        return user;
    }
}
